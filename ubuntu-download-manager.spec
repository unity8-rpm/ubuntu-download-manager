%global debug_package %{nil}

Name:		ubuntu-download-manager
Version:	r2535
Release:	1%{?dist}
Summary:	Provides a service for downloading files while an application is suspended

Group:		Applications
License:	LGPL
URL:		https://github.com/ubports/ubuntu-download-manager
Source0:	https://github.com/abhishek9650/ubuntu-download-manager/archive/%{name}-%{version}.tar.gz
Patch0:		RemoveQtArg.patch
Patch1:		Gtest.patch

BuildRequires:	boost-devel, dbus-test-runner, doxygen, cmake, gtest-devel, qt5-devel
Requires:	qt5, qt5-declarative, libnih, glog
BuildRequires:	pkgconfig(dbus-1)
BuildRequires:	pkgconfig(libglog)
BuildRequires:	pkgconfig(libnih-dbus)

%description
Provides a service for downloading files while an application is suspended.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
%cmake 	-DCMAKE_INSTALL_PREFIX:PATH=/usr  \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir}  \
	-DCMAKE_INSTALL_LIBEXECDIR=%{_libexecdir}
make %{?_smp_mflags}

#Tests are broken
#%check
#make check


%install
%make_install


##Note: Files needs to go into subpackages, But since it is alpha stage, i am leaving them here
%files
%doc COPYING
%{_sysconfdir}/dbus-1/system.d/*.conf
%{_bindir}/ubuntu-upload-manager
%{_bindir}/ubuntu-download-manager
%{_incudedir}/ubuntu/transfers/*.h
%{_incudedir}/ubuntu/transfers/errors/*.h
%{_includedir}/ubuntu/upload_manager/*.h
%{_includedir}/ubuntu/download_manager/*.h

%{_libdir}/*.so.*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/qt/qml/Ubuntu/DownloadManager/qmldir
%{_libdir}/qt/qml/Ubuntu/DownloadManager/libUbuntuDownloadManager.so
%{_libdir}/systemd/user/ubuntu-download-manager-systemd.service
%{_libdir}/%{name}/udm-extractor
%{_datadir}/dbus-1/services/*.service
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/doc/%{name}/cpp/html/*
%{_datadir}/doc/%{name}/cpp/html/search/*


%changelog
